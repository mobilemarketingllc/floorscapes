# Beaver Builder Mega Menu #
**Contributors:** [BeaverBuilderMegaMenu](https://profiles.wordpress.org/BeaverBuilderMegaMenu)  
**Requires at least:** 4.0.0  
**Tested up to:** 5.2  
**Stable tag:** 1.0.5  
**Requires PHP:** 5.6  
**License:** GPLv2 or later  

Beaver Builder Mega Menu is modular element for creating Mega Menus on your site made with Beaver Builder.

## Changelog ##

### 1.0.5 ###
* Fixes a compatibility issue between other plugins using common names to generate cryptographic nonces.

### 1.0.4 ###
* Fixes an issue where updating the plugin on a site using a transient object cache would result in an error.

### 1.0.3 ###
* Fixes a compatibility issue between other plugins using EDD.

### 1.0.2 ###
* Improves clarity of error message when attempting to activate plugin on unsupported versions of PHP.

### 1.0.1 ###
* Fixes an issue where animations bound to the window didn't trigger after page load.

### 1.0 ###
* Initial release.